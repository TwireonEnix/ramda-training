const R = require('ramda');
const { expect } = require('chai');

/**
 * Wraps a function of any arity (also nullarity) in a function that accepts
 * exactly n parameters. Others won't be passed.
 * Number -> (* -> a) -> (* -> a)
 */
it('R.nAry', () => {
	const max4Args = R.nAry(4, (...args) => args);
	expect(max4Args(1, 2, 3, 4, 5, 6)).to.deep.equal([ 1, 2, 3, 4 ]);
});
