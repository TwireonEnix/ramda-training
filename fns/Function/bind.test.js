const R = require('ramda');
const { expect } = require('chai');

// 1 Using prototypes
function Greeter (phrase) {
	this.phrase = phrase;
}

Greeter.prototype.greet = function (name) {
	return this.phrase + name;
};

const helloEr = new Greeter('Hello, ');

// 2 Regular object
const Greeter2 = {
	init (phrase) {
		return Object.assign({}, this, { phrase });
	},
	greet (name) {
		return this.phrase + name;
	}
};

const helloEr2 = Greeter2.init('Welcome ');

/**
 * Returns a function that is bound to a context
 * (* -> *) -> {*} -> (* -> *)
 */
it('R.bind', () => {
	const message = R.compose(
		R.toUpper,
		// we use the function that will be composed passing along its instantiated context
		R.bind(helloEr.greet, helloEr)
	)('John');
	expect(message).to.equal('HELLO, JOHN');

	// maybe we could use bind to pass the mongoose model?
	//also i need to learn how to compose promises.
	const message2 = R.bind(helloEr2.greet, helloEr2);
	expect(message2('Doe')).to.equal('Welcome Doe');
});
