const R = require('ramda');
const { expect } = require('chai');

/**
 * Performs rtl function composition, The rightmost fn may have any arity,
 * remaining must be unary
 * ((y -> z), (x -> y), ..., (o -> p), ((a, b, ..., n) -> o)) -> ((a, b, ..., n) -> z)
 * These topics are far more deep in theory, but in practice I got a lot of experience
 * with composing curried functions.
 */
it('R.compose', () => {
	const addEx = R.flip(R.concat)('!');
	const upper = x => x.toUpperCase();
	const word = 'Hello';
	const upperAndExc = addEx(upper(word));

	const composeUpperAndEx = R.compose(upper, addEx);
	expect(composeUpperAndEx(word)).to.equal(upperAndExc);
});

/**
 * Performs ltr function composition.
 * (((a, b, ..., n) -> o), (o -> p), ..., (y -> z), (x -> y)) -> ((a, b, ..., n) -> z)
 */
it('R.pipe', () => {
	const inc = R.add(1);
	const dbl = R.multiply(2);
	const fn = R.pipe(R.add, inc, dbl, inc, inc, dbl);

	expect(fn(6, 4)).to.equal(48);
});
