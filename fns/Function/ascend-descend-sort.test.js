const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a comparator function out of a function that returns a value that
 * can be compared with < and >, in ascending or descending order.
 * Ord b => (a -> b) -> a -> a -> Number
 */
it('R.ascend and R.descend', () => {
	const p1 = { age: 20, name: 'c' };
	const p2 = { age: 30, name: 'a' };
	const p3 = { age: 15, name: 'b' };
	const byAgeAsc = R.ascend(R.prop('age'));
	const byNameDesc = R.descend(R.prop('name'));
	const ascendAge = R.compose(R.pluck('age'), R.sort(byAgeAsc));
	const descendName = R.compose(R.pluck('name'), R.sort(byNameDesc));

	expect(ascendAge([ p1, p2, p3 ])).to.deep.equal([ 15, 20, 30 ]);
	expect(descendName([ p1, p2, p3 ])).to.deep.equal([ 'c', 'b', 'a' ]);
});
