const R = require('ramda');
const { expect } = require('chai');

/**
 * Applies a function to the argument list args. This is used to create
 * a fixed-arity function from a variadic function
 * (*... -> a) -> [*] -> a
 */
it('R.apply', () => {
	const data = [1, 2, 3, 4, 2, 10, -3];
	const fixedMax = R.apply(Math.max);
	expect(fixedMax(data)).to.equal(10);
	// In other words it will convert a variadic function into a function that
	// accepts a list as its parameter.
	expect(Math.max(...data)).to.equal(10);
});

/**
 * Inverse of apply, It returns a variadic function from a function that
 * takes an array, which:
 * - takes any number of positional args
 * - passes these args to fn as an array,
 * - returns the result.
 * ([*...] -> a) -> (*... -> a)
 */
it('R.unapply', () => {
	const variadicJSON = R.unapply(JSON.stringify);
	expect(variadicJSON(1, 2, 3, 4, 5)).to.equal('[1,2,3,4,5]');
});
