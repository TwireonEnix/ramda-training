const R = require('ramda');
const { expect } = require('chai');

/** Creates a new iteration function from an existing one by adding
 * two new parameters to the cb fn, the index and the entire list
 * This is used to for example make the ramda map to behave more or less
 * like the native array.prototype.map.
 * ((a... -> b) ... -> [a] -> *) -> ((a..., Int, [a] -> b)) ... -> [a] -> *
 */
it('R.addIndex', () => {
	const power2 = x => x ** 2;
	const mapIndexed = R.addIndex(R.map);
	const poweredArray = mapIndexed((val, i) => R.add(power2(i), val), [1, 2, 3]);
	expect(poweredArray).to.eql([1, 3, 7]);
});
