const R = require('ramda');
const { expect } = require('chai');

/** Wraps a function of any arity (including nullary) in a function
 * that accepts exactly 1 parameter, others won't be passed
 * (* -> b) -> (a -> b)
 */
it('R.unary', () => {
	const maxOneArgs = R.unary((...args) => args);
	expect(maxOneArgs(1, 2, 3, 4, 5, 6)).to.deep.equal([ 1 ]);
});
