const R = require('ramda');
const { expect } = require('chai');

/**
 * Type signature is really convoluted:
 * ((x1, x2, ...) -> z) -> [((a, b, ...) -> x1), ((a, b, ...) -> x2), ...]
 * -> (a -> b -> ... -> z)
 *
 * Accepts a converging function and a list of branching functions, and returns
 * a new function. When invoked, this new function is applied to some args, each
 * branching fn is applied to those same arguments. The results of each branching
 * function are passed as arguments to the converging function to produce a return value
 *
 * So there's a converging fn, a list of branching fns, and we get a fn back, so when we
 * pass an argument to the returned fn, that argument is passed to all the branching fns,
 * preserving order, then in the same order as the branching list, the return value of
 * every branching function is passed as the argument(s) to the converging function.
 *
 * All branching functions should be of the same arity, provided there was no partial
 * application here.
 */
it('R.converge', async () => {
  // average example from the docs, the array is passed to the sum and length fns
  // then their yielded values are passed to divide: R.divide(R.sum(arr), R.length(arr));
  const avg = R.converge(R.divide, [R.sum, R.length]);
  expect(avg([0, 1, 2, 3, 4, 5, 6, 7, 8])).to.equal(4);

  const b2 = x => x ** 2;
  const b3 = x => x ** 3;
  const b4 = x => x ** 4;
  const c = (a, b, c) => a + b + c;

  const addPower = R.converge(c, [b2, b3, b4]);
  expect(addPower(2)).to.equal(28);
});
