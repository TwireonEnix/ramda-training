const R = require('ramda');
const { expect } = require('chai');

/** Acts a placeholder returning a function that expects the remaining argument. */
it('R.__', () => {
	const subtract = R.curry((a, b) => a - b);
	const subtract10 = subtract(R.__, 10);
	const anotherExample = subtract(R.__, 15);
	expect(subtract10(20)).to.equal(10);
	expect(anotherExample(30)).to.equal(15);
});
