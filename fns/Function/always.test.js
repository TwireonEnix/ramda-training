const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function that always returns the given value
 * a -> (* -> a)
 * K combinator in SKI combinator calculus
 * Also known as the constant function.
 */
it('R.always', () => {
  //vanilla implementation
  const k = x => () => x;
  const b = k(10)('abcd');

  const c = R.always(10)();
  expect(b).to.equal(c);
});
