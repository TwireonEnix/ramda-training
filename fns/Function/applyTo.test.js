const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function that wraps a value that is expecting a function to be
 * applied to the value.
 * Also known as a the 'thrush' combinator
 * a -> (a -> b) -> b
 */
it('R.applyTo', () => {
	const v42 = R.applyTo(42);
	const incremented = v42(R.inc);
	expect(incremented).to.equal(43);
	expect(v42(R.multiply(2))).to.equal(84);
	expect(v42(R.identity)).to.equal(42);
});
