const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a comparator function out of a function that reports whether the first element is
 * less than the second
 * ((a, b) -> Boolean) -> ((a, b) -> Number)
 */
it('R.comparator', () => {
	const h1 = { height: 100 };
	const h2 = { height: 150 };
	const h3 = { height: 20 };
	const heights = [ h1, h2, h3 ];

	const byHeight = R.comparator((a, b) => a.height < b.height);
	const sortByHeight = R.sort(byHeight);
	expect(sortByHeight(heights)).to.deep.equal([ h3, h1, h2 ]);
	[ 1, 2, 3 ].map(R.cond).filter(R.identity);
});
