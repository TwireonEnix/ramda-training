const R = require('ramda');
const { expect } = require('chai');

/** Wraps a function of any arity (including nullary) in a function
 * that accepts exactly two parameters (not curried), the rest won't be passed.
 * (* -> c) -> (a, b -> c)
 */
it('R.binary', () => {
	const maxTwoArgs = R.curry(R.binary(Math.max));
	const max = maxTwoArgs(10)(2, 5, 20);
	expect(max).to.equal(10);
});
