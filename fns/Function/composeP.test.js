const R = require('ramda');
const { expect } = require('chai');

const articles = [
  {
    id: 1,
    title: 'Post 1',
    description: 'First post',
    author: 'John'
  },
  {
    id: 2,
    title: 'Post 2',
    description: 'Second post',
    author: 'Jane'
  },
  {
    id: 3,
    title: 'Post 3',
    description: 'Third post',
    author: 'Doe'
  }
];

const getArticles = art => Promise.resolve(art);
const getArticle = id => art => Promise.resolve(R.find(R.propEq('id', id), art));

// Deprecated in library
it('R.composeP', async () => {
  const greetAuthor = R.composeP(
    x => new Promise(r => setTimeout(() => r(R.concat('Hello ', x)), 0)),
    R.prop('author'), // can we compose with non promise returning functions? apparently we can
    getArticle(2),
    getArticles
  );
  const author = await greetAuthor(articles);
  expect(author).to.equal('Hello Jane');
});
