const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function which given a specification obj recursively mapping,
 * property to functions produces an obj of the same structure, by mapping
 * each property to the result of calling its property functions with the
 * supplied args.
 * {k: ((a, b, ..., m) -> v)} -> ((a, b, ..., m) -> {k: v})
 */
it('R.applySpec', () => {
	const spec = {
		sum: R.add,
		nested: {
			mul: R.multiply,
			mod: R.modulo,
			moreNest: {
				diff: R.flip(R.subtract),
			},
		},
	};
	const aplSpec = R.applySpec(spec);
	expect(aplSpec(6, 2)).to.deep.equal({
		sum: 8,
		nested: {
			mul: 12,
			mod: 0,
			moreNest: {
				diff: -4,
			},
		},
	});
});
