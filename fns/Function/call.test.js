const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns the result of calling its first argument with the remaining arguments
 * (*... -> a), *... -> a
 *
 * Call and apply are very similar, in fact they do the same thing, however, call
 * accepts variadic arguments, and apply accepts a single array of arguments
 * implemented in vanilla js =>
 * call = fn => (...args) => fn(...args);
 * apply = fn => args => fn([...args]);
 */
it('R.call', () => {
	const inc = R.call(R.add, 1);
	expect(inc(2)).to.equal(3);
});
