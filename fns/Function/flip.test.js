const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a new function like the supplied one, except that
 * the first two parameters order is reversed.
 * ((a, b, c, ...) -> z) -> (b -> a -> c -> ... -> z)
 */
it('R.flip', () => {
	const subtract10 = R.subtract(10);
	const add10 = R.flip(R.subtract)(-10);
	expect(subtract10(10)).to.equal(add10(-10));
});
