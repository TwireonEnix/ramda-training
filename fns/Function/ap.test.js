const R = require('ramda');
const { expect } = require('chai');

/**
 * Ap applies a list of functions to a list of values.
 * [a -> b] -> [a] -> [b]
 * Apply f => f (a -> b) -> fa -> fb
 * (a -> b -> c) -> (a -> b) -> (a -> c)
 *
 * In this notation a -> b -> c, each literal means that they could be the
 * same thing but not necessarily, speaking of types.
 *
 * The length of list of values returned at the end is
 * the multiplication the length of both lists.
 */
it('R.ap', () => {
	const mul2 = R.multiply(2);
	const add3 = R.add(3);
	const ap = R.ap([mul2, add3]);
	expect(ap([3, 4, 5])).to.deep.equal([6, 8, 10, 6, 7, 8]);

	// As stated in the documentation, it can be used as an S combinator
	//(in SKI combinator calculus) when only two functions are passed

	// In this example we can see the use of the final signature where concat
	// is a reducer curried function: (a -> b -> c)
	// toUpper is from the kind of  (a -> b)
	// then ap returns a function that takes the a, and return the c.

	/**
	 * Step by step what happens is that, the argument sent to:
	 * 1. R.concat, which will return a function that expects the final argument,
	 * 2. Is passed then to R.upper, which will return a value,
	 * 3. Then this value will be the second input to the reducer concat
	 * 4. The reducer produces the final value.
	 */
	const rda = R.ap(R.concat, R.toUpper)('ramda');
	expect(rda).to.equal('ramdaRAMDA');

	const transformA = R.ap(R.multiply, R.add(3));
	expect(transformA(4)).to.equal(28);
	expect(transformA(5)).to.equal(40);

	// S combinator implemented with vanilla javascript
	const vanillaAP = (x, y) => z => x(z)(y(z));
	const transformB = vanillaAP(R.multiply, R.add(3));
	expect(transformB(4)).to.equal(28);

	// example with an object
	const obj = {
		data: 'Hello',
		reps: 3,
	};
	const repeatData = R.compose(
		R.repeat,
		R.prop('data'),
	);

	const repeated = R.ap(repeatData, R.prop('reps'));
	expect(repeated(obj)).to.deep.equal(['Hello', 'Hello', 'Hello']);
});
