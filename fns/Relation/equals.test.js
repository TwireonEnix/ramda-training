const R = require('ramda');
const { expect } = require('chai');

/**
 * a -> b -> Boolean
 * Returns true if its args are equivalent, false otherwise. Handles cyclical data
 * structures.
 * Dispatches symmetrically to the equals methods of both arguments if present.
 */
it('R.equals', () => {
  const a = {
    hello: 'World'
  };
  const b = { ...a };

  // due to javascript native object equality behaviour, a is not equal to b, because
  // they point to a different object in memory, although their structures are all the same
  // a problem that ramda solves.
  expect(a === b).to.be.false;
  expect(R.equals(a, b)).to.be.true;
});
