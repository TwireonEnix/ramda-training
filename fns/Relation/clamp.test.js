const R = require('ramda');
const { expect } = require('chai');

/**
 * Restricts a number (or other order-able types such as strings and dates) to be within a range
 * Ord a => a -> a -> a -> a
 */
it('R.clamp', () => {
	const between10And20 = R.clamp(10, 20);
	expect(between10And20(-Infinity)).to.equal(10);
	expect(between10And20(30)).to.equal(20);
});
