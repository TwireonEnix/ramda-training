const R = require('ramda');
const { expect } = require('chai');

/**
 * Logical and of two things, Returns true if both args are true
 * false otherwise
 * a -> b -> a | b
 */
it('R.and', () => {
	const t = R.and(true);
	expect(t(R.T())).to.be.true;
	expect(t(R.F())).to.be.false;
});
