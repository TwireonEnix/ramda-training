const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function which takes a list of predicates that will compose
 * the predicates under the hood, and return a predicate that will return
 * true only in the cases when all the predicates return true.
 * [(*... -> Boolean)] -> (*... -> Boolean)
 */
it('R.allPass', () => {
	const predicate1 = R.flip(R.gt)(10); // equivalent with the predicate 2
	const predicate2 = R.lt(R.__, 20);
	const fn = R.allPass([predicate1, predicate2]);
	expect(R.all(fn, [11, 15, 19])).to.be.true;
	expect(fn(9)).to.be.false;
});
