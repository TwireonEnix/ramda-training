const R = require('ramda');
const { expect } = require('chai');

/**
 * In set theory the complement of a set A is everything that is not in that set.
 * Returns a function g after taking a function f which if called with the same args
 * when f returns a truthy value, g returns a falsy value and the other way around.
 * (*... -> *) -> (*... -> Boolean)
 *
 * Note that complement and not are fairly similar, however 'complement' receives a function
 * and 'not' a value.
 */
it('R.complement', () => {
	const gt10 = n => n > 10;
	const lt10 = R.complement(gt10);
	expect(lt10(9)).to.be.true;
	expect(lt10(11)).to.be.false;

	const isFalsy = R.complement(R.identity);
	expect(isFalsy('')).to.be.true;
	expect(isFalsy('Hello')).to.be.false;
});
