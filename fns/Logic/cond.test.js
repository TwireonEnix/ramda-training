const R = require('ramda');
const { expect } = require('chai');

/**
 * Return a fn which encapsulates if/else, if/else, ... logic,
 *
 * [[(*... -> Boolean), (*... -> *)]] -> (*... -> *)
 *
 * Is the functional equivalent of an imperative switch. The cond function receives a
 * list of [predicate, transformer] pairs.
 * Predicates are functions which yields a Boolean, they can be n-ary.
 * pred :: *... -> Bool
 * transformers are functions which operates on the element and follows the rules of
 * pure functions.
 * transformer :: *... -> *
 *
 * The value passed to the function is run through every predicate until it gets a true,
 * then the same value is passed to the transformer which will yield the final result.
 * If there's no matching predicate, the function will return undefined.
 *
 *
 */

it('R.cond', () => {
  // R.equals, R.identity, R.T and K combinator written in vanilla
  const k = x => () => x;
  const id = x => x;
  const t = k(true);
  const eq = x => y => x === y;

  const hello = R.cond([
    [eq('john'), k('Hello John')],
    [eq('jane'), R.concat('Hello ')],
    [eq('the same'), id],
    [t, k('Hello unknown person')]
  ]);

  expect(hello('john')).to.equal('Hello John');
  expect(hello('')).to.equal('Hello unknown person');
  expect(hello('jane')).to.equal('Hello jane');
  expect(hello('the same')).to.equal('the same');
});
