const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function which calls the two provided predicates and returns
 * their 'and' operation.
 * (*... -> Boolean) -> (*... -> Boolean) -> (*... -> Boolean)
 * This operation is short-circuited (second predicate fn won't be called
 * if first one returns false).
 */
it('R.both', () => {
	const g10 = R.flip(R.gte)(10);
	const l20 = R.flip(R.lte)(20);
	const fn = R.both(g10, l20);
	expect(fn(10)).to.be.true;
	expect(fn(30)).to.be.false;

	// vanilla implementation (however in this implementation the second
	// function will always be called)
	const both = (fn1, fn2) => x => fn1(x) && fn2(x);
	const fn2 = both(g10, l20);
	expect(fn2(15)).to.be.true;
});
