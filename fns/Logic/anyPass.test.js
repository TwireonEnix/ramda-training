const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a function after mixing a list of predicates that will
 * return true with a logical or.
 * [(*... -> Boolean)] -> (*... -> Boolean)
 */
it('R.anyPass', () => {
  const predicate = R.anyPass([R.flip(R.gte)(110), R.flip(R.lte)(100)]);
  expect(predicate([10])).to.be.true;
  expect(predicate([102])).to.be.false;
});
