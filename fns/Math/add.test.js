const R = require('ramda');
const { expect } = require('chai');

/** Curried version of a simple add function
 * Number -> Number -> Number
 */
it('R.add', () => {
  const a = R.add(R.__, 3)(2);
  const add2 = R.add(2);
  expect(a).to.equal(5);
  expect(add2(5)).to.equal(7);
});
