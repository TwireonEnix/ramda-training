const R = require('ramda');
const { expect } = require('chai');

/**
 * Chain maps a function over a list and concatenates the results.
 * Chain m => (a -> mb) -> ma -> mb
 */
it('R.chain', () => {
  const tuple = x => [x, x];
  const data = [1, 2, 3, 4, 5];
  expect(R.chain(tuple, data)).to.eql([1, 1, 2, 2, 3, 3, 4, 4, 5, 5]);
});
