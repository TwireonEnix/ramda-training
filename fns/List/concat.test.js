const R = require('ramda');
const { expect } = require('chai');

/**
 * Return the result of concatenating the given lists or strings
 * [a] -> [a] -> [a]
 * String -> String -> String
 * R.concat is a not a variadic function, when the native concat is. Also native concat allows the
 * concatenation of any type into a single array. R.concat needs to be of the same type as stated in
 * the signature.
 */
it('R.concat', () => {
  const concatenated = R.concat([1, 2, 3], [4, 5, 6], [7]);
  expect(concatenated).to.deep.equal([1, 2, 3, 4, 5, 6]);
});
