const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns true if at least one elements of the list match the
 * predicate, false otherwise. Logical or.
 * (a -> Boolean) -> [a] -> Boolean
 */
it('R.any', () => {
	const eq10 = R.equals(10);
	expect(R.any(eq10, [10, 20, 30])).to.be.true;
});
