const R = require('ramda');
const { expect } = require('chai');

/** Returns a new list by pulling every item out and all its subarrays and
 * putting them in one one-depth field
 * [a] -> [c]
 */
it('R.flatten', () => {
  const tuple = x => [x, x];
  const data = [1, 2];
  const fn = R.compose(R.flatten, R.map(tuple));
  expect(fn(data)).to.eql([1, 1, 2, 2]);
});
