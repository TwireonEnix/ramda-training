const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a new list, composed of n-tuples of consecutive elements
 * Number -> [a] -> [[a]]
 *
 * If Number is greater than length of the array, it will return an
 * empty list
 */
it('R.aperture', () => {
	const empty = R.aperture(100, [1, 2, 3]);
	expect(R.isEmpty(empty)).to.be.true;
	const aoa = R.aperture(1, [1, 2, 3]);
	expect(aoa).to.deep.equal([[1], [2], [3]]);
});
