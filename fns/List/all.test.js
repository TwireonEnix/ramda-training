const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns true if all the elements of a list match the predicate,
 * a predicate is a function that returns a boolean.
 *(a -> Boolean) -> [a] -> Boolean
 */
it('R.addIndex', () => {
  const obj = {
    a: 'something',
    b: 'something else'
  };
  const withEmptiness = {
    ...obj,
    b: undefined
  };
  const fn = o => R.all(R.identity, R.values(o));
  expect(fn(obj)).to.be.true;
  expect(fn(withEmptiness)).to.be.false;
});
