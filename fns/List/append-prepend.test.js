const R = require('ramda');
const { expect } = require('chai');

/**
 * Returns a new list containing the passed list followed by the given
 * element
 * a -> [a] -> [a]
 */
it('R.append', () => {
  const append2 = R.append(2);
  expect(append2([1, 3])).to.deep.equal([1, 3, 2]);
});

/**
 * Returns a new list beginning with the given element followed by the
 * passed list.
 * a -> [a] -> [a]
 */
it('R.prepend', () => {
  const prepend2 = R.prepend(2);
  expect(prepend2([1, 3])).to.deep.equal([2, 1, 3]);
});
