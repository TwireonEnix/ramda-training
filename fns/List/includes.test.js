const R = require('ramda');
const { expect } = require('chai');

/**
 * a -> [a] -> Boolean
 * Predicate which returns true, if the specified value is equals in R.equals terms (internals
 * for this are somewhat convoluted, is a strict with type and value equality),
 * to at least one element of the given List, false otherwise.
 */

// deprecated, We should use includes
it('R.includes', () => {
  const o1 = { foo: 1 };
  const o2 = {
    ...o1,
    bar: { foo: 1 }
  };
  const includeO1 = R.includes(o1);
  expect(includeO1([{ foo: 1 }, {}, { bar: 2 }])).to.be.true;
  expect(includeO1({ foo: 1, bar: 2 })).to.be.false;
  expect(R.includes(1, { foo: 1, bar: 2 })).to.be.false; // only works with lists, as stated in its signature.
  expect(R.includes(o1, [o2])).to.be.false;
  expect(R.includes(o1, [o1])).to.be.true;
});
