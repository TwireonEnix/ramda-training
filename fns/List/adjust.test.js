const R = require('ramda');
const { expect } = require('chai');

/** Return a new array by applying a function to a item on the list
 * specified by its index.
 * Number -> (a -> a) -> [a] -> [a]
 */
it('R.adjust', () => {
	const result = R.adjust(2, R.inc, [0, 1, 2]);
	expect(R.last(result)).to.equal(3);
});
