const R = require('ramda');
const { expect } = require('chai');

/**
 * Creates a deep copy of the value which may contain any number of
 * nested values of arrays, objects, numbers, strings, booleans and dates.
 * Functions are assigned by reference.
 * {*} -> {*}
 */
it('R.clone', () => {
	const o1 = { foo: 'bar', say: name => `Hello ${name}` };
	const o2 = o1;
	const o3 = R.clone(o1);
	expect(o1).to.equal(o2);
	expect(o2).to.not.equal(o3);
	expect(o1).to.deep.equal(o3);
	expect(o3.say('John')).to.equal('Hello John');
	expect(o1.say).to.equal(o3.say);
});
