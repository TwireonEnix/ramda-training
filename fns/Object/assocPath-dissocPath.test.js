const R = require('ramda');
const { expect } = require('chai');

const obj = {
	name: {
		first: 'FIRST',
		last: 'LAST',
	},
	number: 1,
};

/**
 * Makes a shallow clone of an object, setting or overriding nodes (assocPath),
 * or removing them (dissocPath), required to create the given path, and placing or removing
 * the specific value at the tail end of that path. Non-primitive props are copied by reference.
 * [Idx] -> a -> {a} -> {a}
 * Idx = String | Int
 */

it('R.assocPath', () => {
	const overrideName = R.assocPath(['name', 'first']);
	expect(overrideName('NEW', obj)).to.deep.equal({
		name: {
			first: 'NEW',
			last: 'LAST',
		},
		number: 1,
	});
});

/**
 * [Idx] -> {a} -> {a}
 * Idx = String | Int
 */

it('R.dissocPath', () => {
	const removeLastName = R.dissocPath(['name', 'last']);
	expect(removeLastName(obj)).to.deep.equal({
		name: { first: 'FIRST' },
		number: 1,
	});
});
