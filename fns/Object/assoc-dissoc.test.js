const R = require('ramda');
const { expect } = require('chai');

const obj = {
	name: 'foobar',
};

/**
 * Returns a clone of an object setting, overriding (assoc)
 * or removing (dissoc) the given property
 * Non-primitive values are copied by reference.
 * String -> a -> {k: v} -> {k: v}
 */

it('R.assoc', () => {
	const addProp = R.assoc('prop', 'value');
	expect(addProp(obj)).to.deep.equal({
		name: 'foobar',
		prop: 'value',
	});
});

// String -> {k: v} -> {k: v}
it('R.dissoc', () => {
	const addProp = R.dissoc('name');
	expect(addProp(obj)).to.be.empty;
});
